#!/bin/bash

chroot=${chroot:-/mnt}
esp_size=${esp_size:-256MiB}
kernel=${kernel:-linux}
swap_size=${swap_size:-8GiB}


main() {
	if [ "${EUID}" -ne 0 ]; then
		echo "This script must be run as root."
		exit 1
	fi

	[ -b "${disk}" ] || exit
	[ ! "${timezone}" ] || [ -f "/usr/share/zoneinfo/${timezone}" ] || exit
	[ ! "${locale}" ] || ! grep -q "^#\?${locale} " /etc/locale.gen || exit

	local pattern
	pattern='[1-9][[:digit:]]*[KMGTPEZY]iB'
	[[ "${esp_size}" =~ ${pattern} ]] || exit
	[[ "${swap_size}" =~ ${pattern} ]] || exit
	pattern='^linux(|-hardened|-lts|-zen)$'
	[[ "${kernel}" =~ ${pattern} ]] || exit
	pattern='^(|amd|intel)$'
	[[ "${ucode}" =~ ${pattern} ]] || exit

	echo chroot: $chroot
	echo disk: $disk
	echo esp_size: $esp_size
	echo kernel: $kernel
	echo swap_size: $swap_size
	echo timezone: $timezone
	echo ucode: $ucode
	echo

	local input
	echo "Continue? (y/N) "
	read -r input
	if [ ! "${input}" ] || [[ ! "${input}" =~ y ]]; then
		echo abort
		exit
	fi

	exit

	timedatectl set-ntp true

	local guid_esp="c12a7328-f81f-11d2-ba4b-00a0c93ec93b"
	local guid_swap="0657fd6d-a4ab-43c4-84e5-0933c84b4f4f"
	local guid_root="4f68bce3-e8cd-4db1-96e7-fbcaf984b709"
	local guid_home="933ac7e1-2eb4-4f13-b844-0e14e2aef915"
	sfdisk "${disk}" <<- EOF
		label: gpt
		type=${guid_esp}, size=${esp_size}
		type=${guid_swap}, size=${swap_size}
		type=${guid_root}
	EOF
	sleep 1

	local lsblk="lsblk -lnpo NAME,PARTTYPE"
	local dev_boot=$(${lsblk} "${disk}" | grep "${guid_esp}" | cut -d' ' -f1)
	local dev_swap=$(${lsblk} "${disk}" | grep "${guid_swap}" | cut -d' ' -f1)
	local dev_root=$(${lsblk} "${disk}" | grep "${guid_root}" | cut -d' ' -f1)
	local dev_home=$(${lsblk} "${disk}" | grep "${guid_home}" | cut -d' ' -f1)
	mkfs.ext4 "${dev_root}"
	mkdir -p "${chroot}"
	mount "${dev_root}" "${chroot}"
	mkfs.vfat "${dev_boot}"
	mkdir -p "${chroot}/boot"
	mount "${dev_boot}" "${chroot}/boot"
	mkfs.ext4 "${dev_home}"
	mkdir -p "${chroot}/home"
	mount "${dev_home}" "${chroot}/home"
	mkswap "${dev_swap}"

	pacstrap "${chroot}" base "${kernel}" linux-firmware ${ucode:+${ucode}-ucode}

#	genfstab -U "${chroot}" >> "${chroot}/etc/fstab"

	if [ "${timezone}" ]; then
		arch-chroot "${chroot}" ln -sf "/usr/share/zoneinfo/${timezone}" /etc/localtime
	fi
	arch-chroot "${chroot}" hwclock --systohc

	if [ "${locale}" ]; then
		sed -i "/^#${locale} /s/^#//g" "${chroot}/etc/locale.gen"
		arch-chroot "${chroot}" locale-gen
		echo "LANG=${locale}" > "${chroot}/etc/locale.conf"
	fi

	echo "${hostname}" > "${chroot}/etc/hostname"
	cat > "${chroot}/etc/hosts" <<- EOF
		127.0.0.1 localhost
		::1       localhost
		127.0.1.1 ${hostname}.localdomain ${hostname}
	EOF

	mv "${chroot}/etc/mkinitcpio.conf{,.bak}"
	cat > "${chroot}/etc/mkinitcpio.conf" <<- EOF
		HOOKS=(systemd autodetect modconf block filesystems keyboard fsck)
	EOF
	arch-chroot "${chroot}" mkinitcpio -P

	if [ "${root_password}" ]; then
		echo "root:${root_password}" | arch-chroot "${chroot}" chpasswd
	fi

	arch-chroot "${chroot}" bootctl install
	cat > "${chroot}/boot/loader/loader.conf" <<- EOF
		default      arch
		timeout      1
		console-mode max
		editor       no
	EOF
	local resume_uuid=$(lsblk -no UUID ${dev_swap})
	cat > "${chroot}/boot/loader/entries/arch.conf" <<- EOF
		title   Arch Linux
		linux   /vmlinuz-${kernel}${ucode:+
		initrd  /${ucode}-ucode.img}
		initrd  /initramfs-${kernel}.img
		options rw resume=UUID=${resume_uuid}
	EOF

	if [ "${username}" ]; then
		arch-chroot "${chroot}" useradd -m "${username}"
		if [ "${password}" ]; then
			echo "${username}:${password}" | arch-chroot "${chroot}" chpasswd
		fi
	fi

	mv "${chroot}/etc/pacman.conf{,.bak}"
	cat > "${chroot}/etc/pacman.conf" <<- EOF
		[options]
		HoldPkg = pacman glibc
		Architecture = auto
		CheckSpace
		SigLevel = Required DatabaseOptional
		LocalFileSigLevel = Optional

		[core]
		Include = /etc/pacman.d/mirrorlist

		[extra]
		Include = /etc/pacman.d/mirrorlist

		[community]
		Include = /etc/pacman.d/mirrorlist

		[multilib]
		Include = /etc/pacman.d/mirrorlist
	EOF

	arch-chroot "${chroot}" pacman -Syu "${packages[@]}"
}

main "${@}"
